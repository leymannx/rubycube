# RubyCube

Following https://docs.docker.com/compose/rails

### Start the application

1. `git clone https://github.com/leymannx/rubycube.git`
2. `cd rubycube`
3. `docker-compose up -d`
4. `docker-compose run web rake db:create`
5. View the Rails welcome page `http://localhost:3000`

### Stop the application

`docker-compose down`

### Restart the application

1. `docker-compose up -d`
2. `docker-compose run web rake db:create`

### Rebuild the application

If you make changes to the Gemfile or the Compose file to try out some different configurations, you need to rebuild. Some changes require only `docker-compose up -d --build`, but a full rebuild requires a re-run of `docker-compose run web bundle install` to sync changes in the `Gemfile.lock` to the host, followed by `docker-compose up -d --build`.

Here is an example of the first case, where a full rebuild is not necessary. Suppose you simply want to change the exposed port on the local host from `3000` in our first example to `3001`. Make the change to the Compose file to expose port `3000` on the container through a new port, `3001`, on the host, and save the changes:

```
ports: - "3001:3000"
```

Now, rebuild and restart the app with `docker-compose up --build`, then restart the database: `docker-compose run web rake db:create`.

Inside the container, your app is running on the same port as before `3000`, but the Rails Welcome is now available on `http://localhost:3001` on your local host.

# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
